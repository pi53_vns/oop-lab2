﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Write("Лабораторна робота №2.\nВиконала: Волинець Н.С., група ПІ-53\nВаріант №3\nЗавдання 5.\n");
            int x, even = 0, odd = 0, pos = 0, neg = 0;
            Console.WriteLine("Для завершення введення чисел введіть 0");
            do
            {
                Console.Write("Введіть ціле число = ");
                if (!(int.TryParse(Console.ReadLine(), out x)))
                {
                    Console.WriteLine("Помилка при введенні цілого числа. Будь ласка, повторіть введення значення ще раз!");
                    continue;
                }            
            if (x == 0)
                break;
            if (x > 0)
                pos++;
            if (x < 0)
                neg++;
            if (x % 2 == 0)
                even++;
            if (x % 2 != 0)
                odd++;
            } while (true);
            Console.WriteLine("-----------------------------------------");
            Console.Write($"Кількість парних чисел: {even:D}\nКількість непарних чисел: {odd:D}\nКількість додатних чисел: {pos:D}\nКількість від'ємних чисел: {neg:D}\n");
        }
    }
}
