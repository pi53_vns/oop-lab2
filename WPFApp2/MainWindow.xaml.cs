﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFApp2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            float a, b, c;
            double D, x1, x2;
            if (!(float.TryParse(textBoxA.Text, out a)))
            {
                MessageBox.Show("Помилка при введенні значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!(float.TryParse(textBoxB.Text, out b)))
            {
                MessageBox.Show("Помилка при введенні значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!(float.TryParse(textBoxC.Text, out c)))
            {
                MessageBox.Show("Помилка при введенні значення c!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            D = b * b - 4 * a * c;
            textBoxD.Text = D.ToString("F2");
            if (D > 0)
            {
                x1 = (-b - Math.Sqrt(D)) / (2 * a);
                x2 = (-b + Math.Sqrt(D)) / (2 * a);
                label5.Visibility = Visibility.Visible;
                textBoxX1.Visibility = Visibility.Visible;
                label6.Visibility = Visibility.Visible;
                textBoxX2.Visibility = Visibility.Visible;
                textBoxX1.Text = x1.ToString("F2");
                textBoxX2.Text = x2.ToString("F2");
            }
            else if (D == 0)
            {
                x1 = -b / (2 * a);
                label6.Visibility = Visibility.Hidden;
                textBoxX2.Visibility = Visibility.Hidden;
                textBoxX1.Text = x1.ToString("F2");               
            }
            else
            {
                label5.Visibility = Visibility.Hidden;
                textBoxX1.Visibility = Visibility.Hidden;
                label6.Visibility = Visibility.Hidden;
                textBoxX2.Visibility = Visibility.Hidden;
                MessageBox.Show("Розв'язків немає!", "D < 0", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
