﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Write("Лабораторна робота №2.\nВиконала: Волинець Н.С., група ПІ-53\nВаріант №3\nЗавдання 1.\n");
            double x, y, z;
            do {
                Console.Write("Введіть дробове значення х: ");
                if (!(double.TryParse(Console.ReadLine(), out x)))
                    Console.WriteLine("Помилка при введенні значення х. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            do
            {
                Console.Write("Введіть дробове значення y: ");
                if (!(double.TryParse(Console.ReadLine(), out y)))
                    Console.WriteLine("Помилка при введенні значення y. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            do
            {
                Console.Write("Введіть дробове значення z: ");
                if (!(double.TryParse(Console.ReadLine(), out z)))
                    Console.WriteLine("Помилка при введенні значення z. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            double S = (((1.0 + Math.Sin(x + y)) / (Math.Abs(z - 2.0 * x / (1 + Math.Pow(x, 2.0) * Math.Pow(y, 2.0))))) * Math.Pow(x, Math.Abs(y))) + Math.Tan(1.0 / z);
            Console.WriteLine($"Результат обчислення: S = {S:F3}");
        }
    }
}
