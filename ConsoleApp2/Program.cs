﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Write("Лабораторна робота №2.\nВиконала: Волинець Н.С., група ПІ-53\nВаріант №3\nЗавдання 2.\n");
            float a, b, c;
            double D, x1, x2;
            do
            {
                Console.Write("Введіть значення першого коефіцієнта а: ");
                if (!(float.TryParse(Console.ReadLine(), out a)))
                    Console.WriteLine("Помилка при введенні значення a. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            do
            {
                Console.Write("Введіть значення другого коефіцієнта b: ");
                if (!(float.TryParse(Console.ReadLine(), out b)))
                    Console.WriteLine("Помилка при введенні значення b. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            do
            {
                Console.Write("Введіть значення вільного члена c: ");
                if (!(float.TryParse(Console.ReadLine(), out c)))
                    Console.WriteLine("Помилка при введенні значення с. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);

            D = b * b - 4 * a * c;
            if (D > 0)
            {
                x1 = (-b - Math.Sqrt(D)) / (2 * a);
                x2 = (-b + Math.Sqrt(D)) / (2 * a);
                Console.WriteLine($"Результат: D = {D:F3}\nx1 = {x1:F3}\nx2 = {x2:F3}");
            }
            else if (D == 0)
            {
                x1 = -b / (2 * a);
                Console.WriteLine($"Результат: D = {D:F3}\nx = {x1:F3}");
            }
            else
            {
                Console.WriteLine($"Результат: D = {D:F3}\nРозв'язків немає!");
            }
        }
    }
}