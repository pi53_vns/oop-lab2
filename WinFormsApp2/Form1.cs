﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float a, b, c;
            double D, x1, x2;
            if (!(float.TryParse(textBoxA.Text, out a)))
            {
                MessageBox.Show("Помилка при введенні значення a!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!(float.TryParse(textBoxB.Text, out b)))
            {
                MessageBox.Show("Помилка при введенні значення b!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!(float.TryParse(textBoxC.Text, out c)))
            {
                MessageBox.Show("Помилка при введенні значення c!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }            

            D = b * b - 4 * a * c;
            textBoxD.Text = D.ToString("F2");
            if (D > 0)
            {
                x1 = (-b - Math.Sqrt(D)) / (2 * a);
                x2 = (-b + Math.Sqrt(D)) / (2 * a);
                label5.Visible = true;
                textBoxX1.Visible = true;
                label6.Visible = true;
                textBoxX2.Visible = true;
                textBoxX1.Text = x1.ToString("F2");
                textBoxX2.Text = x2.ToString("F2");            
            }
            else if (D == 0)
            {
                x1 = -b / (2 * a);
                label6.Visible = false;
                textBoxX2.Visible = false;                
                textBoxX1.Text = x1.ToString("F2");               
            }
            else
            {
                label5.Visible = false;
                textBoxX1.Visible = false;
                label6.Visible = false;
                textBoxX2.Visible = false;
                MessageBox.Show("Розв'язків немає!", "D < 0", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
