﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Write("Лабораторна робота №2.\nВиконала: Волинець Н.С., група ПІ-53\nВаріант №3\nЗавдання 4.\n");
            uint n, k;
            do
            {
                Console.Write("Введіть ціле додатнє число N = ");
                if (!(uint.TryParse(Console.ReadLine(), out n)))
                    Console.WriteLine("Помилка при введенні значення N. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            do
            {
                Console.Write("Введіть ціле додатнє число K = ");
                if (!(uint.TryParse(Console.ReadLine(), out k)))
                    Console.WriteLine("Помилка при введенні значення K. Будь ласка, повторіть введення значення ще раз!");
                else break;
            } while (true);
            double sum = 0;
            for (int i = 1; i <= n; i++)
                sum += Math.Pow(i, k);
            Console.WriteLine($"Сума: {sum:F0}");
        }
    }
}
