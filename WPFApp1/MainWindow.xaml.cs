﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Load(object sender, RoutedEventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z;
            if (!(double.TryParse(textBoxX.Text, out x)))
            {
                MessageBox.Show("Помилка при введенні значення x!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!(double.TryParse(textBoxY.Text, out y)))
            {
                MessageBox.Show("Помилка при введенні значення y!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!(double.TryParse(textBoxZ.Text, out z)))
            {
                MessageBox.Show("Помилка при введенні значення z!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double S = (((1.0 + Math.Sin(x + y)) / (Math.Abs(z - 2.0 * x / (1 + Math.Pow(x, 2.0) * Math.Pow(y, 2.0))))) * Math.Pow(x, Math.Abs(y))) + Math.Tan(1.0 / z);
            textBoxRes.Text = S.ToString("F2");
        }               
    }
}
