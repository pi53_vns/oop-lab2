﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x, y, z;
            if (!(double.TryParse(textBoxX.Text, out x)))
            {
                MessageBox.Show("Помилка при введенні значення x!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!(double.TryParse(textBoxY.Text, out y)))
            {
                MessageBox.Show("Помилка при введенні значення y!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!(double.TryParse(textBoxZ.Text, out z)))
            {
                MessageBox.Show("Помилка при введенні значення z!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            double S = (((1.0 + Math.Sin(x + y)) / (Math.Abs(z - 2.0 * x / (1 + Math.Pow(x, 2.0) * Math.Pow(y, 2.0))))) * Math.Pow(x, Math.Abs(y))) + Math.Tan(1.0 / z);
            textBoxRes.Text = S.ToString("F2");
        }
    }
}
